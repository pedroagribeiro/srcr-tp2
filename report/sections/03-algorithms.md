## Algoritmos {#sec:algorithms}

### Pesquisa Não Informada

#### Depth First

```prolog
depth_first_engine([[Goal|Path]|_], Goal, [Goal|Path], Filter, Arguments).
depth_first_engine([Path|Queue], Goal, FinalPath, Filter, Arguments) :-
    possible_nextnodes_df(Path, NewPaths, Filter, Arguments),
    append(NewPaths, Queue, NewQueue),
    depth_first_engine(NewQueue, Goal, FinalPath, Filter, Arguments).
```

Comecemos por olhar para o caso de paragem do algoritmo: o algoritmo para assim
que o caminho estiver no frente da lista de possíveis caminhos e assim que o
próximo destino for o destino que pretendemos alcançar.

No corpo do algoritmo estendemos o caminho que temos com os caminhos possíveis
(tendo em conta os nodos que poderemos visitar, ou seja, os adjacentes que
cumprem as condições que pretendemos). Seguidamente, juntamos os novos caminhos
possíveis à lista de caminhos possíveis anterior e continuamos a procura de
forma recursiva.

Para que seja possível filtrar os vértices que serão visitáveis de acordo com
condições arbitrárias que podemos querer definir a função de procura recebe um
`Filter` que é um predicado que permite, precisamente, fazer essa filtragem,
juntamente com um algum argumento que possamos querer utilizar com esse
predicado (`Arguments`).

#### Breadth First

```prolog
breadthfirst_engine(
    Dest/Cost,
    Orig,
    Dest,
    Visited,
    Expanded,
    Path,
    Filter,
    FurtherArgs) :-
    Path = [Orig/Dest/Cost|Visited].

breadthfirst_engine(
    Node/Cost,
    Orig,
    Dest,
    Visited,
    Expanded,
    Path,
    Filter,
    FurtherArgs) :-
    updateFringeBreadthFirst(Node, Filter, FurtherArgs, Expanded, ExpansionResult),
    chooseNextNode(ExpansionResult, Visited, [NodeCost/NodeOrig/NextNode|ExplVisited]),
    updateVisited(Orig/Node/Cost,Visited,[],UpdatedVisited),
    breadthfirst_engine(
        NextNode/NodeCost,
        NodeOrig,
        Dest,
        UpdatedVisited,
        ExplVisited,
        Path,
        Filter,
        FurtherArgs).
```

O caso de paragem deste algoritmo, assim como o anterior, é quando o vértice
que pretendemos alcançar for adicionado ao caminho (o que significa que o
destino foi atingido).

Quando iniciamos a execução do algoritmo passamos `Visited` com uma lista vazia
uma vez que ainda nenhum vértice foi visitado assim como a lista de
vértices expandidos (`Expanded`). `Orig` é passado com o valor não considerado
válido no contexto do problema (por exemplo -1) uma vez que o vértice que
ocupamos aquando da chamada inicial não é consequência de termos visitado
vértice algum anteriormente. Da mesma forma que o algoritmo anterior podia
receber um `Filter` e um `FurtherArguments` este pode igualmente recebê-los
sendo que cumprem exatamente o mesmo propósito.

Relativamente à execução do algoritmo: primeiramente recolhemos a lista de
vértices para os quais podemos transitar com o predicado
`updateFringeBreadthFirst/5`, juntamos a lista dos vértices expandidos aos
possíveis adjacentes que acabamos de encontrar para termos todos os vértices
para os quais podemos transitar.  Seguidamente, escolhemos o próximo nodo a
visitar com o predicado `chooseNextNode/5` e adicionamo-lo à lista de
visitados. Por fim, chamamos o algoritmo de forma recursiva para os valores
atualizados das variáveis controladoras.

### Pesquisa Informada

#### Greedy Search

```prolog
greedy_engine(Dest/Cost, Orig, Dest, Visited, Expanded, Filter, FurtherArgs, R) :-
   R = [Orig/Dest/Cost|Visited], !.
greedy_engine(Node/Cost, Orig, Dest, Visited, Expanded, Filter, FurtherArgs, R) :-
   updateFringe(Node, Filter, FurtherArgs, Expanded, ExpansionResult),
   chooseNextNodeGreedy(
        Node,
        ExpansionResult,
        Visited,
        [NodeCost/NodeOrig/NextNode|ExplVisited]),
   updateVisited(Orig/Node/Cost, Visited, [], UpdatedVisited),
   greedy_engine(
      NextNode/NodeCost,
      NodeOrig,
      Dest,
      UpdatedVisited,
      ExplVisited,
      Filter,
      FurtherArgs,
      R).
```

Este algoritmo segue uma lógica extremamente semelhante ao `Breadth First`, no
entanto, a lista de nodos visitáveis é ordenada tendo em conta apenas o custo
da transição (_CurrentNode -> NextNode_). Esta última alteração é a responsável
pelo próximo vértice a ser escolhido ser aquele que implica menor custo a
partir do vértice em que nos encontramos presentemente.

#### A* Search

```prolog
astar_engine(
    Dest/Cost,
    From,
    Dest,
    Visited,
    Expanded,
    History,
    Filter,
    FurtherArgs) :-
    History = [Dest/From/Cost|Visited], !.
astar_engine(
    Node/Cost,
    From,
    Dest,
    Visited,
    Expanded,
    History,
    Filter,
    FurtherArgs) :-
    updateFringeAStar(
        Node,
        Cost,
        Dest,
        Expanded,
        ToExpand,
        Filter,
        FurtherArgs,
        ExpansionResult),
    chooseNextNodeAStar(
        Node,
        ExpansionResult,
        Visited,
        [CostToDest/NextNode/FromNode/CostToNode|NextIterExpanded]),
    updateVisitedAStar(
        Node/From/Cost,
        Visited,
        [],
        UpdatedVisited),
    astar_engine(
        NextNode/CostToNode,
        FromNode,
        Dest,
        UpdatedVisited,
        NextIterExpanded,
        History,
        Filter,
        FurtherArgs).

```

Este algoritmo segue igualmente a mesma lógica do `Breadth First` e do `Greedy
Search` sendo que na transição tem em conta a soma do custo da transição para o
próximo vértice e uma estimativa do custo a partir do vértice presente até ao
vértice destino do algoritmo. A estimativa até ao destino consiste no cálculo
da distância euclidiana do vértice presente até ao vértice destino através do
predicado `estimate/3`.

