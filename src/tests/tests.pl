% vim: syntax = prolog

%---------------------------------------------------------------------------------------------------
% INCLUDES -----------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- include('test_graph.pl').
:- include('../utils/depthfirst_search.pl').
:- include('../utils/breadthfirst_search.pl').
:- include('../utils/greedy_search.pl').
:- include('../utils/astar_search.pl').

%------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais ---------------------------------------
%------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).