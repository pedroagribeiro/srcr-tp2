% vim: syntax=prolog

%---------------------------------------------------------------------------------------------------
% INCLUDES -----------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- include('filters.pl').
:- include('maths.pl').
:- include('print_utils.pl').

%---------------------------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais -------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).
:- use_module(library(lists)).
:- use_module(library(system)).

%---------------------------------------------------------------------------------------------------
% DEPTH FIRST SEARCH ALGORITHM ---------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

updateFringeBreadthFirst(Node, Filter, FurtherArgs, Expanded, NewFringe) :-
    expandFringe(Node, Filter, FurtherArgs, NewFringeNodes),
    append(Expanded, NewFringeNodes, NewFringe).

bf_solver(Orig, Orig, Path, Filter, FurtherArgs) :-
    Path = [Orig],!.
bf_solver(Orig, Dest, Path, Filter, FurtherArgs) :-
    breadthfirst_engine(Orig/0, -1, Dest, [], [], ToExtract, Filter, FurtherArgs),
    buildPathFromVisited(Orig, Dest, ToExtract, InvPath),
    reverse(InvPath, Path),!.


chooseNextNode([Cost/NodeOrig/Node|T], Visited, Path) :-
   isFringeNode(Node, Visited),
   chooseNextNode(T, Visited, Path);
   Path = [Cost/NodeOrig/Node|T].

breadthfirst_engine(Dest/Cost, Orig, Dest, Visited, _, Path, Filter, FurtherArgs) :-
    Path = [Orig/Dest/Cost|Visited].
breadthfirst_engine(Node/Cost, Orig, Dest, Visited, Expanded, Path, Filter, FurtherArgs) :-
    updateFringeBreadthFirst(Node, Filter, FurtherArgs, Expanded, ExpansionResult),
    chooseNextNode(ExpansionResult,Visited,[NodeCost/NodeOrig/NextNode|ExplVisited]),
    updateVisited(Orig/Node/Cost,Visited,[],UpdatedVisited),
    breadthfirst_engine(
        NextNode/NodeCost,
        NodeOrig,
        Dest,
        UpdatedVisited,
        ExplVisited,
        Path,
        Filter,
        FurtherArgs).


%---------------------------------------------------------------------------------------------------
% QUERY 1 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query1breadthfirst(Origem, Destino) :-
    statistics(walltime, [_|T1]),
    bf_solver(Origem, Destino, R, 'none', undefined),
    pathCost(R, Custo),
    statistics(walltime, [_|T2]),
    prettify(R),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 2 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query2breadthfirst(Origem, Destino, Operadoras) :-
    statistics(walltime, [_|T1]),
    bf_solver(Origem, Destino, R, 'operator', Operadoras),
    pathCost(R, Custo),
    statistics(walltime, [_|T2]),
    prettify(R),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 3 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query3breadthfirst(Origem, Destino, Operadoras) :-
    statistics(walltime, [_|T1]),
    bf_solver(Origem, Destino, R, 'not_operator', Operadoras),
    pathCost(R, Custo),
    statistics(walltime, [_|T2]),
    prettify(R),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 4 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query4breadthfirst(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   bf_solver(Origem, Destino, R, 'none', undefined),
   pathCost(R, Custo),
   bindCarrierCount(R, BindedCountList),
   getMaxCarrierCount(BindedCountList, SortedCarrierList),
   getFrontRunner(SortedCarrierList, MaxCarrier),
   filterByMaxCarrier(MaxCarrier, BindedCountList, Result),
   statistics(walltime, [_|T2]),
   prettifyPairList(Result),
   printCost(Custo),
   printTime(T2).


%---------------------------------------------------------------------------------------------------
% QUERY 5 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query5breadthfirst(Origem, Destino) :-
    statistics(walltime, [_|T1]),
    bf_solver(Origem, Destino, R, 'none', undefined),
    pathCost(R, Custo),
    statistics(walltime, [_|T2]),
    prettify(R),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 6 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query6breadthfirst(Origem, Destino) :-
    statistics(walltime, [_|T1]),
    bf_solver(Origem, Destino, R, 'none', undefined),
    pathCost(R, Custo),
    statistics(walltime, [_|T2]),
    prettify(R),
    printCost(Custo),
    printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 7 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query7breadthfirst(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   advertised(Origem, undefined),
   advertised(Destino, undefined),
   bf_solver(Origem, Destino, R, 'advertised', undefined),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 8 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query8breadthfirst(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   sheltered(Origem, undefined),
   sheltered(Destino, undefined),
   bf_solver(Origem, Destino, R, 'sheltered', undefined),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 9 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query9breadthfirst(NodeList) :-
   statistics(walltime, [_|T1]),
   actualPathBuilder(NodeList, 'bf_solver', Result),
   statistics(walltime, [_|T2]),
   prettify(Result),
   pathCost(Result, Cost),
   printCost(Cost),
   printTime(T2).