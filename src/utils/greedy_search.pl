% vim: syntax=prolog

%---------------------------------------------------------------------------------------------------
% INCLUDES -----------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- include('filters.pl').
:- include('maths.pl').
:- include('print_utils.pl').

%---------------------------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais -------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).
:- use_module(library(lists)).
:- use_module(library(system)).

%---------------------------------------------------------------------------------------------------
% GREEDY SEARCH ALGORITHM --------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------


chooseNode(Orig/Node/Cost, [NodeOrig/Node/NodeCost|T], Seen, Result) :-
   Cost < NodeCost,
   TempResult = [Orig/Node/Cost|T],
   append(TempResult, Seen, Result);
   Cost >= NodeCost,
   TempResult = [NodeOrig/Node/NodeCost|T],
   append(TempResult, Seen, Result).

updateVisited(Orig/Node/Cost, [], AlreadyChecked, R):-
   R = [Orig/Node/Cost|AlreadyChecked].
updateVisited(Orig/Node/Cost, [NodeOrig/Node/NodeCost|T], AlreadyChecked, R) :-
   chooseNode(Orig/Node/Cost, [NodeOrig/Node/NodeCost|T], AlreadyChecked, R).
updateVisited(Orig/Node/Cost, [NodeOrig/NodeID/NodeCost|T], AlreadyChecked, R) :-
   updateVisited(Orig/Node/Cost, T, [NodeOrig/NodeID/NodeCost|AlreadyChecked], R).

isFringeNode(Orig, []):-
   fail,!.
isFringeNode(Node, [Origin/Node/Cost|Tail]).
isFringeNode(Orig, [Origin/Node/Cost|Tail]) :-
   Orig =\= Node,
   isFringeNode(Orig, Tail).

buildPathFromVisited(Orig, Dest, Visited, Path) :-
   VisitedCopy = Visited,
   pathBuilderFromVisited(Visited, Orig, Dest, VisitedCopy, Path).

pathBuilderFromVisited([Orig/Dest/_|_], Orig, Dest, _, Result) :-
   Result = [Dest, Orig].
pathBuilderFromVisited([NodeOrig/Dest/_|T], Orig, Dest, VisitedCopy, [Dest|Result]) :-
   pathBuilderFromVisited(VisitedCopy, Orig, NodeOrig, VisitedCopy, Result).
pathBuilderFromVisited([H|T], Orig, Dest, VisitedCopy, Result) :-
   pathBuilderFromVisited(T, Orig, Dest, VisitedCopy, Result).

expandFringe(Orig, Filter, FurtherArgs, R) :-
   getPossibleExpansions(Orig, [], Filter, FurtherArgs, Unsorted),
   sort(Unsorted, R), !.

getPossibleExpansions(Orig, Visited, Filter, FurtherArgs, R) :-
   call(Filter, Dest, FurtherArgs),
   adjacencia(Orig, Dest, Cost, _),
   \+ member(Cost/Orig/Dest, Visited),
   getPossibleExpansions(Orig, [Cost/Orig/Dest|Visited], Filter, FurtherArgs, R),!;
   R = Visited, !.

greedy_solver(Orig, Orig, Path, _, _) :-
   Path = [Orig], !.
greedy_solver(Orig, Dest, Path, Filter, FurtherArgs) :-
   greedy_engine(Orig/0, undefined, Dest, [], [], Filter, FurtherArgs, ToExtract),
   buildPathFromVisited(Orig, Dest, ToExtract, InvPath),
   reverse(InvPath, Path),!.

chooseNextNodeGreedy(CurrStop, [Cost/NodeOrig/Node|Tail], Visited, R) :-
   \+ isFringeNode(Node, Visited),
   CurrStop = NodeOrig,
   R = [Cost/NodeOrig/Node|Tail];
   chooseNextNodeGreedy(CurrStop, Tail, Visited, R).

updateFringe(Node, Filter, FurtherArgs, Expanded, NewFringe) :-
   expandFringe(Node, Filter, FurtherArgs, NewFringeNodes),
   append(NewFringeNodes, Expanded, UnsortedFringe),
   sort(UnsortedFringe, NewFringe).

greedy_engine(Dest/Cost, Orig, Dest, Visited, _, Filter, FurtherArgs, R) :-
   R = [Orig/Dest/Cost|Visited], !.
greedy_engine(Node/Cost, Orig, Dest, Visited, Expanded, Filter, FurtherArgs, R) :-
   updateFringe(Node, Filter, FurtherArgs, Expanded, ExpansionResult),
   chooseNextNodeGreedy(Node, ExpansionResult, Visited, [NodeCost/NodeOrig/NextNode|ExplVisited]),
   updateVisited(Orig/Node/Cost, Visited, [], UpdatedVisited),
   greedy_engine(
      NextNode/NodeCost,
      NodeOrig,
      Dest,
      UpdatedVisited,
      ExplVisited,
      Filter,
      FurtherArgs,
      R).

%---------------------------------------------------------------------------------------------------
% QUERY 1 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query1greedy(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   greedy_solver(Origem, Destino, R, 'none', undefined),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 2 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query2greedy(Origem, Destino, Operadoras) :-
   statistics(walltime, [_|T1]),
   greedy_solver(Origem, Destino, R, 'operator', Operadoras),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 3 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query3greedy(Origem, Destino, Operadoras) :-
   statistics(walltime, [_|T1]),
   greedy_solver(Origem, Destino, R, 'not_operator', Operadoras),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 4 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query4greedy(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   greedy_solver(Origem, Destino, R, 'none', undefined),
   pathCost(R, Custo),
   bindCarrierCount(R, BindedCountList),
   getMaxCarrierCount(BindedCountList, SortedCarrierList),
   getFrontRunner(SortedCarrierList, MaxCarrier),
   filterByMaxCarrier(MaxCarrier, BindedCountList, Result),
   statistics(walltime, [_|T2]),
   prettifyPairList(Result),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 5 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

% O método de resolução desta query é utilizar Breadth-First.

%---------------------------------------------------------------------------------------------------
% QUERY 6 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query6greedy(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   greedy_solver(Origem, Destino, R, 'none', undefined),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 7 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query7greedy(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   advertised(Origem, undefined),
   advertised(Destino, undefined),
   greedy_solver(Origem, Destino, R, 'advertised', undefined),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 8 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query8greedy(Origem, Destino) :-
   statistics(walltime, [_|T1]),
   sheltered(Origem, undefined),
   sheltered(Destino, undefined),
   greedy_solver(Origem, Destino, R, 'sheltered', undefined),
   pathCost(R, Custo),
   statistics(walltime, [_|T2]),
   prettify(R),
   printCost(Custo),
   printTime(T2).

%---------------------------------------------------------------------------------------------------
% QUERY 9 ------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

query9greedy(NodeList) :-
   statistics(walltime, [_|T1]),
   actualPathBuilder(NodeList, 'greedy_solver', Result),
   statistics(walltime, [_|T2]),
   prettify(Result),
   pathCost(Result, Cost),
   printCost(Cost),
   printTime(T2).