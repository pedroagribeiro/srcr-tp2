% vim: syntax = prolog

%---------------------------------------------------------------------------------------------------
% SICStus PROLOG: Declarações iniciais -------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

:- set_prolog_flag( discontiguous_warnings,off ).
:- set_prolog_flag( single_var_warnings,off    ).
:- set_prolog_flag( unknown,fail               ).
:- op( 900,xfy,'::' ).

%---------------------------------------------------------------------------------------------------
% Calculates euclidian distance. -------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

distance(Current, Goal, Result) :-
    paragem(Current, Lat1, Long1, _, _, _, _, _, _, _, _),
    paragem(Goal, Lat2, Long2, _, _, _, _, _, _, _, _),
    XGap is Lat1 - Lat2,
    YGap is Long1 - Long2,
    Result is sqrt(XGap^2 + YGap^2).

% Linear estimation
estimate(Current, Goal, Result) :-
    distance(Current, Goal, Result).